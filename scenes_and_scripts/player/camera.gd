extends Camera

export var look_speed: int = 2
var mouse_speed: int = 1
#export var mouse_look_speed: int = 5

func _ready():	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	var mouse_motion = event as InputEventMouseMotion
	if mouse_motion:
		var current_tilt: float = rotation_degrees.y
		current_tilt -= (mouse_motion.relative.x * mouse_speed)/PlayerManager.mouse_sensitivity
		#print(PlayerManager.mouse_sensitivity)
		
		rotation_degrees.y = clamp(current_tilt, 90, 270)
		
func _process(delta):
	if Input.is_action_pressed("truck_look_left"):
		rotation_degrees.y = clamp(rotation_degrees.y+look_speed, 90, 270) 
	if Input.is_action_pressed("truck_look_right"):
		rotation_degrees.y = clamp(rotation_degrees.y-look_speed, 90, 270)
		
#export var min_distance = 2.0
#export var max_distance = 4.0
#export var angle_v_adjust = 0.0
#
#var collision_exception = []
#export var height = 1.5
#
#func _ready():
#	# Find collision exceptions for ray.
#	var node = self
#	while(node):
#		if (node is RigidBody):
#			collision_exception.append(node.get_rid())
#			break
#		else:
#			node = node.get_parent()
#
#	# This detaches the camera transform from the parent spatial node.
#	set_as_toplevel(true)
#
#
#func _physics_process(_delta):
#	var target = get_parent().get_global_transform().origin
#	var pos = get_global_transform().origin
#
#	var from_target = pos - target
#
#	# Check ranges.
#	if from_target.length() < min_distance:
#		from_target = from_target.normalized() * min_distance
#	elif from_target.length() > max_distance:
#		from_target = from_target.normalized() * max_distance
#
#	from_target.y = height
#
#	pos = target + from_target
#
#	look_at_from_position(pos, target, Vector3.UP)
#
#	# Turn a little up or down
#	var t = get_transform()
#	t.basis = Basis(t.basis[0], deg2rad(angle_v_adjust)) * t.basis
#	set_transform(t)
