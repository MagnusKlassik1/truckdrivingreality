extends Spatial

export(NodePath) onready var _cab = get_node(_cab) as VehicleBody

export var engine_force_forward: int = 50
export var engine_force_reverse: int = 5
export var brake_strength: int = 5
export var steering_speed: float = 0.4
export var steering_limit: float = 0.6
export var cruise_control_min_kmh: int = 15
export var cruise_control_increase_step: int = 2
export var cruise_control_decrease_step: int = 2
export var damage_amount_under_ten_kmh: int = 5
export var damage_amount_under_twenty_kmh: int = 10
export var damage_amount_under_fifty_kmh: int = 20
export var damage_amount_above_kmh: int = 30

signal test_signal

func _ready():
	_cab.engine_force_value = engine_force_forward
	_cab.engine_force_value_reverse = engine_force_reverse
	_cab.brake_strength = brake_strength
	_cab.steer_speed = steering_speed
	_cab.steer_limit = steering_limit
	_cab.cruise_control_min = cruise_control_min_kmh
	_cab.increase_cruise_control_by = cruise_control_increase_step
	_cab.decrease_cruise_control_by = cruise_control_decrease_step
	
	# Damage
	PlayerManager.damage_amount_under_ten = damage_amount_under_ten_kmh
	PlayerManager.damage_amount_under_twenty = damage_amount_under_twenty_kmh
	PlayerManager.damage_amount_under_fifty = damage_amount_under_fifty_kmh
	PlayerManager.damage_amount_above = damage_amount_above_kmh



func _on_AICarStopperArea_body_entered(body):
	body.get_parent().set_physics_process(true)


func _on_AICarStopperArea_body_exited(body):
	body.get_parent().set_physics_process(false)
