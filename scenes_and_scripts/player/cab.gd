extends VehicleBody


#onready var reverse_audio = $ReverseAudio
var driving_audio_playing = false

# Steering
var steer_speed: float = 0
var steer_limit: float = 0

var steer_target = 0
# Engine
var engine_force_value: int = 0
var engine_force_value_reverse: int = 0
var brake_strength: int = 0
var current_speed_in_kmh
# Cruise control
var cruise_control_min: int = 0
var increase_cruise_control_by: int = 0
var decrease_cruise_control_by: int = 0
var cruise_control_activated = false
var cruise_control_speed
var cruise_control_speed_int
# Fatalities
var old_speed = 0
# 
var handbrake_activated: bool = false
var reverse_gear_possible: bool = false
# Gas
var gas_timer_running = false
onready var gas_timer = $GasTimer
onready var _motor_audio = $MotorAudio
onready var _throttle_audio = $ThrottleAudio
onready var _throttle_timer = $ThrottleAudio/ThrottleTimer
onready var _reverse_audio = $ReverseAudio

var throttle_sound_allowed = true

var first_mission_given = false


func _ready():
	gas_timer.start(gas_timer.time_left)
	brake = 2
	MissionManager.connect("start_motor", self, "_start_motor")

func _physics_process(delta):
	current_speed_in_kmh = linear_velocity.length()*3.6
	PlayerManager.current_km_per_hour = int(current_speed_in_kmh)
	#var fwd_mps = transform.basis.xform_inv(linear_velocity).z
	steer_target = Input.get_action_strength("truck_steering_left") \
			- Input.get_action_strength("truck_steering_right")
	steer_target *= steer_limit
	steering = move_toward(steering, steer_target, steer_speed * delta)

	if current_speed_in_kmh < old_speed-5:
		var speed_difference = old_speed-current_speed_in_kmh
		PlayerManager.accident(speed_difference)
	old_speed = current_speed_in_kmh
	
	# Driving
	if Input.is_action_pressed("truck_throttle"):
		if _reverse_audio.playing:
			_reverse_audio.stop()
		reverse_gear_possible = false
		#reverse_audio.playing = false
		if handbrake_activated:
			brake = 2
		else:
			engine_force = engine_force_value
			if !_throttle_audio.playing:
				if throttle_sound_allowed:
					_throttle_audio.play()
	elif cruise_control_activated == false:
		engine_force = 0
		_throttle_audio.stop()
	
		
	if PlayerManager.gas < 1:
		engine_force_value = 0
		engine_force_value_reverse = 0
		_motor_audio.stop()
		get_tree().change_scene("res://scenes_and_scripts/level/game_lost.tscn")
		
	# Cruise Control
	if Input.is_action_just_pressed("truck_cruise_control"):
		if current_speed_in_kmh >= cruise_control_min:
			if cruise_control_activated == false:
				_cruise_control(true)
				if !_throttle_audio.playing:
					if throttle_sound_allowed:
						_throttle_audio.play()
			else:
				_cruise_control(false)
			cruise_control_speed = current_speed_in_kmh
			cruise_control_speed_int = int(current_speed_in_kmh)
			

	# Brake
	if Input.is_action_pressed("truck_brake"):
		_cruise_control(false)
		if reverse_gear_possible:
			engine_force = -engine_force_value_reverse
			if !_reverse_audio.playing:
				_reverse_audio.play()
		brake = brake_strength
	else:
		if handbrake_activated == false:
			brake = 0.0
	
	# Activate reverse gear
	if Input.is_action_just_released("truck_brake"):
		if int(current_speed_in_kmh) == 0:
			reverse_gear_possible = true
	
	# Handbrake
	if Input.is_action_just_pressed("truck_handbrake"):
		if handbrake_activated == false:
			brake = 2
			handbrake_activated = true
			engine_force_value_reverse = 0
			PlayerManager.parking_brake_activated = true
			$TruckHUD/CenterContainer/Parking_Brake.show()
		elif handbrake_activated == true:
			brake = 0
			handbrake_activated = false
			engine_force_value_reverse = 5
			PlayerManager.parking_brake_activated = false
			$TruckHUD/CenterContainer/Parking_Brake.hide()
	# Cruise control logic
	if cruise_control_activated == true:
		if cruise_control_speed >= current_speed_in_kmh:
			engine_force = engine_force_value
		if cruise_control_speed <= current_speed_in_kmh:
			engine_force = 0
		if Input.is_action_just_pressed("truck_increase_cruise_control"):
			cruise_control_speed += increase_cruise_control_by
		if Input.is_action_just_pressed("truck_decrease_cruise_control"):
			cruise_control_speed -= decrease_cruise_control_by
			# accident
		if current_speed_in_kmh < cruise_control_speed-20 or Input.is_action_pressed("truck_throttle"):
			_cruise_control(false)
	# Gas logic
	if current_speed_in_kmh != 0:
		if gas_timer_running == false:
			gas_timer.paused = false
			gas_timer_running = true
	if current_speed_in_kmh == 0:
		gas_timer.paused = true
		#gas_timer.stop()
		gas_timer_running = false


func _on_GasTimer_timeout():
	PlayerManager.add_gas(-1)


func _cruise_control(boolean: bool):
	cruise_control_activated = boolean
	PlayerManager.cruise_control(boolean)


func _on_ThrottleAudio_finished():
	throttle_sound_allowed = false
	_throttle_timer.start(2)


func _on_ThrottleTimer_timeout():
	throttle_sound_allowed = true


func _start_motor():
	_motor_audio.play()
