extends Spatial

export(NodePath) onready var _hunger_ticker = get_node(_hunger_ticker) as Timer
export(NodePath) onready var _thirst_ticker = get_node(_thirst_ticker) as Timer
export(NodePath) onready var _bladder_ticker = get_node(_bladder_ticker) as Timer
export(NodePath) onready var _fatigue_ticker = get_node(_fatigue_ticker) as Timer
export(NodePath) onready var _offroad_ticker = get_node(_offroad_ticker) as Timer


signal one_kilometer_done
signal hud_food_eaten
signal hud_drink_drunk

var kilometer_signal_sent: bool = false

onready var truck_body = $Truck/Cab
onready var _iso_camera = $Truck/IsoCamera
onready var _cabin_camera = $Truck/Cab/CabinCamera
onready var player_local_pos = $Truck/Cab.translation
onready var player_global_pos = $Truck/Cab.global_transform.origin
onready var _life_timer = $Life
onready var _speeding_audio = $SpeedingAudio

##################################
### Needs
# Reduction
export var life_decrease_per_minute: float = 5
export var hunger_decrease_per_minute: float = 5
export var thirst_decrease_per_minute: float = 10
export var bladder_decrease_per_minute: float = 7
export var fatigue_decrease_per_minute: float = 3
# Restoration
export var hunger_restored_by_food: int = 30
export var bladder_decrease_by_food: int = 10
export var thirst_restored_by_drinks: int = 20
export var bladder_decrease_by_drinks: int = 15
export var sleep_restored_per_sleep_hour: int = 13
# Fines
export var offroad_fine: int = 10
export var offroad_finde_after_time: int = 5
# Distance
#export var meters_after_getting_paid: int = 1000
export var money_getting_paid: float = 0.28
export var expected_meters_per_second: float = 0.2
var distance_traveled: float = 0

onready var _eating_sound = $Eating
onready var _drinking_sound = $Drinking
onready var radio = $Truck/Cab/Radio

var needs_unfullfilled = 0
var hunger_missing: bool = false
var thirst_missing: bool = false
var bladder_missing: bool = false
var fatigue_missing: bool = false
var life_timer_running: bool = false

func _ready():
	PlayerManager.sleep_restored_per_hour = sleep_restored_per_sleep_hour
	MissionManager.expected_meters_per_second = expected_meters_per_second
	_hunger_ticker.start(60/hunger_decrease_per_minute)
	_thirst_ticker.start(60/thirst_decrease_per_minute)
	_bladder_ticker.start(60/bladder_decrease_per_minute)
	_fatigue_ticker.start(60/fatigue_decrease_per_minute)
	MissionManager.connect("mission_succeed", self, "_add_money")
	#PlayerManager.connect("reduce_life", self, "_handle_life_reducing_timer")
	PlayerManager.connect("offroad", self, "_offroad_fine")


func _process(_delta):
	# We need to get the first player position and add constantly the
	# truck position because player pos is static
	PlayerManager.local_player_position = truck_body.transform.origin
	PlayerManager.global_player_position = truck_body.global_transform.origin
	PlayerManager.player_rotation = truck_body.rotation
	PlayerManager.player_rotation_degrees = truck_body.rotation_degrees
	
	_iso_camera.translation = Vector3(truck_body.transform.origin.x-20, \
	truck_body.transform.origin.y+40, truck_body.transform.origin.z-35)
	
	PlayerManager.distance_driven += truck_body.linear_velocity.length()*_delta

	
	# Controls
	if Input.is_action_just_pressed("ui_switch_camera"):		
		if _iso_camera.current == false:
			_iso_camera.make_current()
		else:
			_cabin_camera.make_current()

	if Input.is_action_just_pressed("player_towing_service"):
		PlayerManager.change_money(-25)
#		translation = Vector3(1,1,-20)
#		$Truck.translation = Vector3(1,1,-20)
		truck_body.gravity_scale = 0
		truck_body.translation = Vector3(1,2,-20)
		
#		truck_body.sleeping = true
#		$Truck/Trailer.sleeping = true

	# Eating and Drinking
	if Input.is_action_just_pressed("player_eat"):
		PlayerManager.eat_food(hunger_restored_by_food, bladder_decrease_by_food)
		_eating_sound.play()
		emit_signal("hud_food_eaten")
	if Input.is_action_just_pressed("player_drink"):
		PlayerManager.consume_drinks(thirst_restored_by_drinks, bladder_decrease_by_drinks)
		_drinking_sound.play()
		emit_signal("hud_drink_drunk")

	# Radio
	if Input.is_action_just_pressed("player_radio"):
		if !radio.playing:
			radio.play()
		else:
			radio.stop()

# Old function where you'd getting paid per kilometer
#func give_money_after_thousand_meters():
#	if int(distance_traveled)%meters_after_getting_paid == 0:
#		# Else player gets money at the beginning
#		if distance_traveled > 1:
#			if kilometer_signal_sent == false:
#				PlayerManager.change_money(money_getting_paid)
#				$RewardAudio.play()
#				kilometer_signal_sent = true
#	else:
#		if kilometer_signal_sent:
#			kilometer_signal_sent = false

func _offroad_fine(value):
	if value == true:
		_offroad_ticker.start(offroad_finde_after_time)
	if value == false:
		_offroad_ticker.stop()


func _add_money():
	var amount = money_getting_paid*MissionManager.distance
	amount = stepify(amount, 0.01)
	PlayerManager.change_money(amount)


func _on_Hunger_timeout():
	PlayerManager.add_hunger(-1)
	hunger_missing = _needs_fullfilled_handler(PlayerManager.hunger, hunger_missing)
	_check_needs_fullfilled()


func _on_Thirst_timeout():
	PlayerManager.add_thirst(-1)
	thirst_missing = _needs_fullfilled_handler(PlayerManager.thirst, thirst_missing)
	_check_needs_fullfilled()


func _on_Bladder_timeout():
	PlayerManager.add_bladder(-1)
	bladder_missing = _needs_fullfilled_handler(PlayerManager.bladder, bladder_missing)
	_check_needs_fullfilled()


func _on_Fatigue_timeout():
	PlayerManager.add_fatigue(-1)
	fatigue_missing = _needs_fullfilled_handler(PlayerManager.fatigue, fatigue_missing)
	_check_needs_fullfilled()


func _handle_life_reducing_timer(boolean):
	if boolean == true:
		_life_timer.start(60/life_decrease_per_minute)
	else:
		_life_timer.stop()


func _on_Life_timeout():
	PlayerManager._add_life(-1)


func _on_Offroad_timeout():
	PlayerManager.change_money(-offroad_fine)
	_speeding_audio.play()


func _check_needs_fullfilled():
	#print(needs_unfullfilled)
	if needs_unfullfilled != 0:
		if life_timer_running == false:
			_life_timer.start(60/life_decrease_per_minute)
			life_timer_running = true
	else:
		if life_timer_running:
			_life_timer.stop()
			life_timer_running = false


func _needs_fullfilled_handler(need, need_missing_bool):
	if need <= 0:
		if need_missing_bool == false:
			needs_unfullfilled += 1
			need_missing_bool = true
			return need_missing_bool
	else:
		if need_missing_bool:
			need_missing_bool = false
			needs_unfullfilled -= 1
			return need_missing_bool
	return need_missing_bool
