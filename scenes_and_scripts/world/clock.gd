extends Node2D

signal time_changed

var MINUTES_PER_HOUR = 60
var HOURS_PER_DAY = 24

export var minute_duration: int
onready var timer = $Timer

var current_time = {"minute": 0, "hour": 0, "day": 1}

func _ready():
	timer.autostart = true
	timer.wait_time = minute_duration

func get_current_time():
	return current_time

func get_current_time_as_string():
	return "%02d:%02d. Day: %d" % [current_time["hour"],
									current_time["minute"],
									current_time["day"]]

func _on_Timer_timeout():
	_increment_minute()
	emit_signal("time_changed")

func _increment_minute():
	_set_time(add_time(0, 0, 1))

func _set_time(new_time):
	current_time = new_time

func add_time(days, hours, minutes):
	hours += int(current_time["minute"] + minutes) / int(MINUTES_PER_HOUR)
	days += int(current_time["hour"] + hours) / int(HOURS_PER_DAY)
	minutes = int(current_time["minute"] + minutes) % int(MINUTES_PER_HOUR)
	hours = int(current_time["hour"] + hours) % int(HOURS_PER_DAY)
	days = int(current_time["day"] + days)
	var result_time = {"minute": minutes, "hour": hours, "day": days}
	return result_time
