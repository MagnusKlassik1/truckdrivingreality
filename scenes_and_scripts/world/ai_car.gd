extends PathFollow

var car_body = preload("res://scenes_and_scripts/world/car_body.tscn")

onready var raycast = $RayCast
onready var raycast2 = $RayCast2
onready var delete_timer = $DeleteTimer
onready var _horns_sound = $Horns
onready var _horning_allowed: bool = true
onready var _horn_timer = $HornTimer
# Speed
export var max_default_speed = 6
export var min_default_speed = 4
export var max_accelleration = 0.2
export var min_accelleration = 0.07
export var spawn_probability: float = 0.5
export var delete_after_seconds_idling: int = 45
var accelleration = 1
var default_speed = 3
var current_speed = 0
var reduced_speed = 0
#
# Collisions:
var horn_reasion_collision_layer = 5
var ai_car_collision_layer = 2
var oneway_collision_layer = 8
var tcrossing_collision_layer = 10
var crossing_collision_layer = 11
var traffic_light_collision_layer = 12
#var path_collision_layer = 13
var traffic_block_collision_layer = 14
# Waypoints
var old_path
var new_path 
# Signal
signal end_reached
var car_is_stopped = false
var destroyer
var raycast_list = []

var colors = [Color.darkgoldenrod, Color.chocolate, Color.cornflower, Color.gray, 
Color.rebeccapurple, Color.darkolivegreen, Color.lightpink, Color.black]

func _ready():
	randomize()
	colors.shuffle()
	#set_physics_process(false)
	# Give random colors
	var car_instance = car_body.instance()
	car_instance.translation = Vector3.ZERO
	var mat_one = car_instance.get_child(0).get_surface_material(2)
	var mat_two = car_instance.get_child(0).get_surface_material(3)
	var car_color =  colors[int(rand_range(0,6))]
	mat_one.albedo_color = colors[0]
	mat_two.albedo_color = colors[0]
	add_child(car_instance)
	# Raycasts
	raycast_list.append(raycast)
	raycast_list.append(raycast2)
	_stop()
	$Driving.play()
	# Randomize car spawn at beginning
	var do_not_spawn_car = rand_range(0,1)
	if do_not_spawn_car > spawn_probability:
		queue_free()

	# Mandatory to prevent new_path from being null
	new_path = self.get_parent()
	unit_offset = rand_range(0.1, 0.9)
	# Speed config
	accelleration = rand_range(min_accelleration, max_accelleration)
	default_speed = rand_range(min_default_speed, max_default_speed)
	current_speed = default_speed


func _physics_process(delta):
	# Check for car collision
	for raycast_unit in raycast_list:
		if raycast_unit.is_colliding():
			_check_ray_collisions(raycast_unit)
	# Movement
	offset += current_speed * delta
	if unit_offset == 1:
		emit_signal("end_reached")
	_drive()


func _check_ray_collisions(raycast_unit):
	randomize()
	var collision_object = raycast_unit.get_collider()
	# Check path blocker
	if collision_object.get_collision_layer_bit(traffic_block_collision_layer):
		_stop()
	# Get new paths
	if collision_object.get_collision_layer_bit(oneway_collision_layer):
		new_path = collision_object.get_child(1)
	# Crossing
	if collision_object.get_collision_layer_bit(crossing_collision_layer):
		var path_number = int(rand_range(1,4))
		new_path = collision_object.get_child(path_number)
	#T-Crossing
	if collision_object.get_collision_layer_bit(tcrossing_collision_layer):
		var path_number = int(rand_range(1,3))
		new_path = collision_object.get_child(path_number)
	if collision_object.get_collision_layer_bit(horn_reasion_collision_layer):
		if _horning_allowed:
			_horns_sound.play()
			_horning_allowed = false
			_horn_timer.start(rand_range(0, 5))

func _on_PathCar_end_reached():
	# Remove old path and apply new path
	old_path = self.get_parent()
	old_path.remove_child(self)
	new_path.add_child(self)
	unit_offset = 0
	# Check if car reaches next path. If not, it is stuck and will be deleted
	delete_timer.start(delete_after_seconds_idling)

# Here we could add a speed so that in curves the car would move slower e.g.
func _drive():
	if current_speed < default_speed:
		current_speed += accelleration
		
func _stop():
	current_speed = 0
#	if current_speed > 0:
#		current_speed -= accelleration

func _reduce_speed(others_speed):
	current_speed = others_speed.current_speed - 0.5
	
func _keep_speed():	
	current_speed = default_speed

func _on_DeleteTimer_timeout():
	queue_free()

func _on_KinematicBody_body_entered(body):
	body.get_collision_layer()

func _on_IndicatorTimer_timeout():
	pass # Replace with function body.


func _on_HornTimer_timeout():
	_horning_allowed = true
