extends Node

signal time_changed

var clock = preload("res://scenes_and_scripts/world/clock.tscn").instance()


func _ready():
	add_child(clock)

func _add_time_to_world(hour, minute):
	clock.current_time["hour"] += hour
	clock.current_time["minute"] += minute

func get_current_time_as_string():
	return clock.get_current_time_as_string()

func get_current_day():
	return clock.get_current_time()["day"]

func get_current_hour():
	return clock.get_current_time()["hour"]

func get_current_minute():
	return clock.get_current_time()["minute"]
