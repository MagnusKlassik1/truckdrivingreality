extends Node

signal speeding(value)
signal offroad(value)
signal accident
signal reduce_life(boolean)
signal game_lost
signal punishment
signal cruise_control(boolean)
signal hud_money_changed(before, after)
signal hud_rest_room_used(before, after)
signal hud_rest_area_used(before, after)
signal hud_gas_changed(before, after)


# Truck
var global_player_position 
var local_player_position
var player_position_cell
var player_rotation
var player_rotation_degrees
var damage_report: float = 100
var is_parking = false
var current_location: String
var current_adress: String
var is_visible: bool = true
var is_on_street: bool = true
var current_company_tile
var current_km_per_hour
var parking_brake_activated = false
var distance_driven = 0

# Injected by Truck
var damage_amount_under_ten
var damage_amount_under_twenty
var damage_amount_under_fifty
var damage_amount_above

# Speeding logic
var current_allowed_speed = 50
var city_speeding_count: int = 0
var highway_speeding_count: int = 0
var max_speeding_count = 6
# Injected by CitySpeed- and HighwaySpeed-Scene
var city_speeding_fine
var highway_speeding_fine

# Inventory
var current_money: float = 10
var current_food: int = 2
var current_drinks: int = 2


# Needs
var max_needs_value = 100
var min_needs_value = 0
var hunger: float = 100
var thirst: float = 100
var bladder: float = 100
var fatigue: float = 100
var life_status: float = 100
var life_timer_running: bool = false
var sleep_restored_per_hour = 5
var gas: float = 100

# Diverse
var current_camera_zoom
var missions_failed = 0

# Game options
var mouse_sensitivity: int = 5

# Functions
func add_to_city_speeding_count(number):
	city_speeding_count = clamp(city_speeding_count + number, 0, max_speeding_count)
	if city_speeding_count == max_speeding_count:
		change_money(-city_speeding_fine)
		# Signal for HUD
		emit_signal("speeding", city_speeding_fine)
		city_speeding_count = 0
		emit_signal("punishment")


func add_to_highway_speeding_count(number):
	highway_speeding_count = clamp(highway_speeding_count + number, 0, max_speeding_count)
	if highway_speeding_count == max_speeding_count:
		change_money(-highway_speeding_fine)
		# Signal for HUD
		emit_signal("speeding", highway_speeding_fine)
		highway_speeding_count = 0
		emit_signal("punishment")


func add_failed_mission():
	missions_failed += 1
	#print(missions_failed)
	#if missions_failed > 3:
	#	emit_signal("game_lost")


func accident(speed_difference):
	var damage_value = 0
	if speed_difference < 10:
		damage_value = damage_amount_under_ten
	if speed_difference >= 10 and speed_difference < 20:
		damage_value = damage_amount_under_twenty
	if speed_difference >= 20 and speed_difference < 50:
		damage_value = damage_amount_under_fifty
	if speed_difference >= 50:
		damage_value = damage_amount_above
	damage_report = clamp(damage_report-abs(damage_value), 0, 100)
	emit_signal("accident", damage_report)
	if damage_report <= 0:
		get_tree().change_scene("res://scenes_and_scripts/level/game_lost.tscn")
		#emit_signal("game_lost")


func cruise_control(boolean):
	emit_signal("cruise_control", boolean)


func player_on_street(check):
	if check == true:
		# Check hier or the signal will be sent every tile
		#if is_on_street == false:
		is_on_street = true
		emit_signal("offroad", false)
	else:
		is_on_street = false
		emit_signal("offroad", true)


func change_money(amount):
	var money_before = current_money
	current_money += amount
	var money_afterwards = current_money
	emit_signal("hud_money_changed", money_before, money_afterwards)


func add_fatigue(value):
	var before = fatigue
	fatigue = clamp(fatigue + value, min_needs_value, max_needs_value)
	var afterwards = fatigue
	emit_signal("hud_rest_area_used", before, afterwards)
	if fatigue <= 0:
		emit_signal("reduce_life", true)
		life_timer_running = true
	else:
		if life_timer_running:
			life_timer_running = false
			emit_signal("reduce_life", false)


func add_thirst(value):
	thirst = clamp(thirst + value, min_needs_value, max_needs_value)
	if thirst <= 0:
		emit_signal("reduce_life", true)
		life_timer_running = true
	else:
		if life_timer_running:
			life_timer_running = false
			emit_signal("reduce_life", false)


func add_hunger(value):
	hunger = clamp(hunger + value, min_needs_value, max_needs_value)
	if hunger <= 0:
		emit_signal("reduce_life", true)
		life_timer_running = true
	else:
		if life_timer_running:
			life_timer_running = false
			emit_signal("reduce_life", false)
	
	
func add_bladder(value):
	var before = bladder
	bladder = clamp(bladder + value, min_needs_value, max_needs_value)
	var after = bladder
	emit_signal("hud_rest_room_used", before, after)
	if bladder <= 0:
		emit_signal("reduce_life", true)
		life_timer_running = true
	else:
		if life_timer_running:
			life_timer_running = false
			emit_signal("reduce_life", false)


func _add_life(value):
	life_status = clamp(life_status + value, min_needs_value, max_needs_value)
	if life_status <= 0:
		get_tree().change_scene("res://scenes_and_scripts/level/game_lost.tscn")
		#emit_signal("game_lost")


func add_gas(value):
	var before = gas
	gas = clamp(gas + value, min_needs_value, max_needs_value)
	var after = gas
	emit_signal("hud_gas_changed", before, after)


func repair(value):
	var before = damage_report
	damage_report = clamp(damage_report + value, min_needs_value, max_needs_value)
	var after = damage_report
	#emit_signal("hud_gas_changed", before, after)

# Eating and Drinking
func add_food():
	current_food += 1


func eat_food(restore_value, bladder_reduction):
	if current_food > 0:
		current_food -= 1
		add_hunger(restore_value)
		add_bladder(-bladder_reduction)


func add_drink():
	current_drinks += 1
	
	
func consume_drinks(restore_value, bladder_reduction):
	if current_drinks > 0:
		current_drinks -= 1
		add_thirst(restore_value)
		add_bladder(-bladder_reduction)


func sleep(hours):
	var sum = sleep_restored_per_hour*hours
	add_fatigue(sum)


func _towing_service():
	pass
