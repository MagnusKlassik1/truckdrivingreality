extends Node

signal calculate_ETA
signal update_mission(current, start, target_place, distance)
signal finish_mission
signal start_motor

signal mission_succeed
signal mission_failure

onready var mission_clock = preload("res://scenes_and_scripts/world/clock.tscn").instance()
#var adress_dict = {}
onready var adress_list = []
#var current_mission
#var current_destination_position 

# Mission
var start
var target_place
var current_destination
var wares
var ETA
var distance: float = 0
var distance_left: float = 0
var expected_meters_per_second

# Mission logic
var car_is_in_company: bool = false

const FARM_TOWN = 0
const REST_AREA = 1
const CAR_COMPANY = 2
const WOODS = 3
const BIG_CITY = 4
const STARTING_COMPANY = 5


func _ready():	
	add_child(mission_clock)
	# Mission clock does not need to run
	mission_clock.timer.stop()


func interact():
	# Only important for first mission
	if current_destination == start:
		current_destination = target_place
		# Necessary to update navigation
		update_mission_navigation()
		_calculate_ETA()
	else:
		# Give player money
		# Start new mission
		_mission_conclusion()
		start_new_mission()
		#emit_signal("finish_mission")
		
func _mission_conclusion():
	if WorldManager.get_current_day() < mission_clock.get_current_time()["day"]: 
		print("Day")
		_mission_success()
	elif WorldManager.get_current_hour() < mission_clock.get_current_time()["hour"]:
		print("Hour")
		_mission_success()
	elif WorldManager.get_current_minute() <= mission_clock.get_current_time()["minute"]:
		print("Minute")
		_mission_success()
	else:
		_mission_failure()
		
		
func _mission_failure():
	emit_signal("mission_failure")


func _mission_success():
	emit_signal("mission_succeed")


func start_first_mission():
	#adress_list.shuffle()
	# Starting place of company
	start = adress_list[STARTING_COMPANY]
	target_place = adress_list[FARM_TOWN]
	current_destination = target_place
	update_mission_navigation()
	_calculate_ETA()
	emit_signal("start_motor")
	print(adress_list)


func start_new_mission():
	adress_list.shuffle()
	start = current_destination
	target_place = adress_list[0] #(0, adress_list.size()))]
	# The player will immediately receive a new target
	current_destination = target_place
	if start == target_place:
		start_new_mission()
	update_mission_navigation()
	_calculate_ETA()
	print(target_place)


func update_mission_navigation():
	emit_signal("update_mission", current_destination, start, target_place, distance)


func _calculate_ETA():
	emit_signal("calculate_ETA")
