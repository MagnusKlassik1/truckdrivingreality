extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var _anim_player = $AnimationPlayer
onready var _credits_music = $CreditsMusic

# Called when the node enters the scene tree for the first time.
func _ready():
	_anim_player.play("Rolling")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	_credits_music.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("player_close"):
		get_tree().change_scene("res://scenes_and_scripts/level/main_menu.tscn")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Rolling":
		get_tree().change_scene("res://scenes_and_scripts/level/main_menu.tscn")
