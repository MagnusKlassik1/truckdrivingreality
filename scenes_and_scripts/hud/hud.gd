extends Control

# Alternative: export(NodePath) onready var _speed_label = get_node(_speed_label) as Label
export(NodePath) onready var _truck_pos_arrow = get_node(_truck_pos_arrow) as TextureRect
export(NodePath) onready var _life_status = get_node(_life_status) as TextureProgress

# Paths
# Car information paths
export(NodePath) var _speed_label_path
export(NodePath) var _minimap_camera_path
export(NodePath) var _cruise_texture_path
export(NodePath) var _cruise_path
export(NodePath) var _gas_progress_path
export(NodePath) var _gas_path
export(NodePath) var _mechanic_progress_path
export(NodePath) var _mechanic_path

# Mission information paths
#export(NodePath) var _mission_header_path
#export(NodePath) var _mission_container_path
export(NodePath) var _mission_time_path
export(NodePath) var _eta_path
export(NodePath) var _distance_left_path
export(NodePath) var _distance_path

# Player information paths
#export(NodePath) var _player_header_path
#export(NodePath) var _player_container_path
export(NodePath) var _money_path
export(NodePath) var _drink_path
export(NodePath) var _food_path
#export(NodePath) var _traveled_path

# Needs information paths
#export(NodePath) var _needs_header_path
#export(NodePath) var _needs_container_path
#export(NodePath) var _hunger_path
export(NodePath) var _hunger_progress_path
#export(NodePath) var _thirst_path
export(NodePath) var _thirst_progress_path
#export(NodePath) var _bladder_path
export(NodePath) var _bladder_progress_path
#export(NodePath) var _fatigue_path
export(NodePath) var _fatigue_progress_path

# Popups paths
export(NodePath) var _speeding_path
export(NodePath) var _speeding_label_path
export(NodePath) var _offroad_path
export(NodePath) var _accident_path
export(NodePath) var _accident_label_path
export(NodePath) var _timer_path

# Sound paths
export(NodePath) var _sound_cruise_path

# Onready
# Car information
onready var _speed: Label = get_node(_speed_label_path)
onready var _minimap_camera: Camera = get_node(_minimap_camera_path)
onready var _cruise_texture: TextureRect = get_node(_cruise_texture_path)
onready var _cruise: Label = get_node(_cruise_path)
onready var _gas_progress: TextureProgress = get_node(_gas_progress_path)
onready var _gas: Label = get_node(_gas_path)
onready var _mechanic_progress: TextureProgress = get_node(_mechanic_progress_path)
onready var _mechanic: Label = get_node(_mechanic_path)

# Mission information paths
#onready var _mission_header: Label = get_node(_mission_header_path)
#onready var _mission_container: VBoxContainer = get_node(_mission_container_path)
onready var _mission_time: Label = get_node(_mission_time_path)
onready var _eta: Label = get_node(_eta_path)
onready var _distance_left: Label = get_node(_distance_left_path)
onready var _distance: Label = get_node(_distance_path)

# Player information paths
#onready var _player_header: Label = get_node(_player_header_path)
#onready var _player_container: VBoxContainer = get_node(_player_container_path)
onready var _money: Label = get_node(_money_path)
onready var _drink: Label = get_node(_drink_path)
onready var _food: Label = get_node(_food_path)
#onready var _traveled: Label = get_node(_traveled_path)

# Needs information paths
#onready var _needs_header: Label = get_node(_needs_header_path)
#onready var _needs_container: VBoxContainer = get_node(_needs_container_path)
#onready var _hunger: Label = get_node(_hunger_path)
onready var _hunger_progress: TextureProgress = get_node(_hunger_progress_path)
#onready var _thirst: Label = get_node(_thirst_path)
onready var _thirst_progress: TextureProgress = get_node(_thirst_progress_path)
#onready var _bladder: Label = get_node(_bladder_path)
onready var _bladder_progress: TextureProgress = get_node(_bladder_progress_path)
#onready var _fatigue: Label = get_node(_fatigue_path)
onready var _fatigue_progress: TextureProgress = get_node(_fatigue_progress_path)

# Popups paths
onready var _speeding: Popup = get_node(_speeding_path)
onready var _speeding_label: Label = get_node(_speeding_label_path)
onready var _offroad: Popup = get_node(_offroad_path)
onready var _accident: Popup = get_node(_accident_path)
onready var _accident_label: Label = get_node(_accident_label_path)
onready var _timer: Timer = get_node(_timer_path)

#############################################
# Minimap
var current_minimap_zoom_level = 1
export var minimap_rotation: bool = true

# Sounds
onready var _cruise_sound: AudioStreamPlayer = get_node(_sound_cruise_path)

# Sound bools
var sound_is_played: bool = false

# HUD
onready var _animation_player = $AnimationPlayer
onready var _punishment_audio = $PunishmentAudio


func _ready():
	PlayerManager.connect("cruise_control", self, "_cruise_control")
	PlayerManager.connect("speeding", self, "_speeding")
	PlayerManager.connect("offroad", self, "_offroad_messsage")
	PlayerManager.connect("accident", self, "_accident")
	PlayerManager.connect("hud_money_changed", self, "_money_changed")
	PlayerManager.connect("hud_rest_room_used", self, "_bladder_changed")
	PlayerManager.connect("hud_rest_area_used", self, "_fatigue_changed")
	PlayerManager.connect("hud_gas_changed", self, "_gas_changed")
	PlayerManager.connect("punishment", self, "_play_punishment_audio")
	current_minimap_zoom_level = 1
	_animation_player.queue("GasChanged")



func _process(delta):
	# Minimap
	_minimap_camera.translation = PlayerManager.global_player_position
	if minimap_rotation:
		_minimap_camera.rotation.y = PlayerManager.player_rotation.y+135
	if minimap_rotation == false:
		_truck_pos_arrow.rect_rotation = -PlayerManager.player_rotation_degrees.y
		
	# Cockpit
	_speed.text = String(PlayerManager.current_km_per_hour)
	_gas_progress.value = PlayerManager.gas
	_mechanic_progress.value = PlayerManager.damage_report
	# Information
	_life_status.value = PlayerManager.life_status
	_hunger_progress.value = PlayerManager.hunger
	_food.text = String(PlayerManager.current_food)
	_thirst_progress.value = PlayerManager.thirst
	_drink.text = String(PlayerManager.current_drinks)
	_bladder_progress.value = PlayerManager.bladder
	_fatigue_progress.value = PlayerManager.fatigue
	_money.text = String(PlayerManager.current_money) + "$"
	_mission_time.text = "CT: " + String(WorldManager.get_current_time_as_string())
	_eta.text = "ETA: " + MissionManager.mission_clock.get_current_time_as_string()
	_distance_left.text = "Route: " + String(stepify(MissionManager.distance_left, 0.1)) + " km"

	if PlayerManager.current_km_per_hour > PlayerManager.current_allowed_speed:
		_speed.add_color_override("font_color", Color.coral)
	else:
		if _speed.get_color("font_color") != Color.white:
			_speed.add_color_override("font_color", Color.white)
			
	if Input.is_action_just_pressed("player_change_minimap_zoom_level"):
		if current_minimap_zoom_level == 1:
			_minimap_camera.size = 300
			current_minimap_zoom_level = 2
		elif current_minimap_zoom_level == 2:
			_minimap_camera.size = 1000
			current_minimap_zoom_level = 4
#		elif current_minimap_zoom_level == 3:
#			_minimap_camera.size = 4000
#			current_minimap_zoom_level = 4
		elif current_minimap_zoom_level == 4:
			_minimap_camera.size = 80
			current_minimap_zoom_level = 1




func _cruise_control(boolean) -> void:
	if boolean == true:
		_cruise_texture.modulate = Color.green
		#_cruise_sound.play()
	else:
		_cruise_texture.modulate = Color.white
		#_cruise_sound.play()

func _offroad_messsage(value):
	if value == true:
		_offroad.show()
	else:
		_offroad.hide()


func _accident(damage_value):
	_accident_label.text = "Accident! Truck status: " + String(PlayerManager.damage_report) + " percent."
	_accident.popup()
	_timer.start()
	
func _speeding(value):
	_speeding_label.text = "Speeding! Fine: " + String(value) + "$"
	_speeding.popup()
	_timer.start()
	#speeding_audio.play()
	#PlayerManager.change_money(-10)

func _on_PopupTimer_timeout():
	_accident.hide()
	_speeding.hide()


func _on_Player_hud_drink_drunk():
	# Coming from player class
	_animation_player.play("DrinkDrunk")
#	if PlayerManager.current_drinks < 1:
#		_drink.add_color_override("font_color", Color.red)


func _on_Player_hud_food_eaten():
	# Coming from player class
	_animation_player.play("FoodEaten")
#	if PlayerManager.current_food < 1:
#		_food.add_color_override("font_color", Color.red)


func _money_changed(before, afterswards):
	if before > afterswards or before == afterswards:
		_animation_player.queue("MoneyChangedNegatively")
	else:
		_animation_player.queue("MoneyChanged")


func _bladder_changed(before, after):
	if after > before:
		_animation_player.queue("BladderChanged")


func _fatigue_changed(before, after):
	if after > before:
		_animation_player.queue("FatigueChanged")


func _gas_changed(before, after):
	if after > before:
		_animation_player.queue("GasChanged")


func _play_punishment_audio():
	_punishment_audio.play()
