extends Area


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_child(1).get_child_count() > 0 or get_child(2).get_child_count() > 0:
		$AreaBlock.set_collision_layer_bit(14, true)
		#$PriorityRight/MeshInstance.show()
		#$CrossingBlock.set_collision_layer_bit(3, true)
		#$Blocker.show()
	else:
		$AreaBlock.set_collision_layer_bit(14, false)
		#$CrossingBlock.set_collision_layer_bit(3, false)
		$Blocker.hide()

