extends GridMap



# Street lanes
var tcrossing_lanes = preload("res://scenes_and_scripts/grid_map/streets/tcrossing_lanes.tscn")
var straight_lanes = preload("res://scenes_and_scripts/grid_map/streets/straight_lanes.tscn")
var crossing_lanes = preload("res://scenes_and_scripts/grid_map/streets/crossing_lanes.tscn")
var curve_lanes = preload("res://scenes_and_scripts/grid_map/streets/curve_lanes.tscn")
var highway_approach_lanes = preload("res://scenes_and_scripts/grid_map/streets/highway_approach_lanes.tscn")
var highway_lanes = preload("res://scenes_and_scripts/grid_map/streets/highway_lanes.tscn")
var highway_big_curve_lanes = preload("res://scenes_and_scripts/grid_map/streets/countryroad_big_curve_lanes.tscn")

# Other
var ai_cars_forbidden_helper = preload("res://scenes_and_scripts/grid_map/traffic/ai_cars_forbidden_helper.tscn")
# GPS
var gps_route_indicator = preload("res://scenes_and_scripts/grid_map/gps/gps_route_indicator.tscn")
var gps_street_component = preload("res://scenes_and_scripts/grid_map/gps/gps_street_component.tscn")
var gps_big_curve_component = preload("res://scenes_and_scripts/grid_map/gps/gps_big_curve_component.tscn")

# Speed Trap
onready var city_speed_limit = preload("res://scenes_and_scripts/grid_map/traffic/city_speed_limit.tscn")
onready var highway_speed_limit = preload("res://scenes_and_scripts/grid_map/traffic/highway_speed_limit.tscn")
# Offroad check
onready var offroad_check_straight = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_straight.tscn")
onready var offroad_check_curve = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_curve.tscn")
onready var offroad_check_tcrossing = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_tcrossing.tscn")
onready var offroad_check_crossing = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_crossing.tscn")
onready var offroad_check_road_straight = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_countryroad_straight.tscn")

# Tile names
const T_CROSSING = 0
const CROSSING = 1
const STRAIGHT = 2
const CURVE = 3
const CROSSROAD = 4
const NO_CARS_STRAIGHT = 5
const NO_CARS_CURVE = 6
const ONE_CLOSED_CROSSING = 7
const HIGHWAY_APPROACH = 9
const HIGHWAY = 10
const HIGHWAY_GRASS = 21
const PLAIN_WITH_SIDEWALK = 13
const PLAIN = 15
const CROSSING_SPLIT_RIGHT = 17
const T_CROSSING_STRAIGHT_ONLY = 19
const COUNTRYROAD_BIG_CURVE = 20

# orientation 
const NORTH = 10
const WEST = 22
const EAST = 16
const SOUTH = 0

# Navigation points
var navigation_points = []

func _ready():
	for cell in get_used_cells():
		# Navigation System
		if get_cell_item(cell.x, cell.y, cell.z) != CROSSING_SPLIT_RIGHT:
			if get_cell_item(cell.x, cell.y, cell.z) == COUNTRYROAD_BIG_CURVE:
				_add_traffic_nodes(cell, gps_big_curve_component)
				#_add_traffic_nodes(cell, highway_big_curve_lanes)
				#_add_traffic_nodes(cell, offroad_check_road_curve)
			else:
				_add_traffic_nodes(cell, gps_street_component)
			#_add_traffic_nodes(cell, offroad_checker)
		#T-Crossing
		if get_cell_item(cell.x, cell.y, cell.z) == T_CROSSING:
			_add_traffic_nodes(cell, tcrossing_lanes)
			_add_traffic_nodes(cell, offroad_check_tcrossing)
			_add_traffic_nodes(cell, city_speed_limit)
		# Crossing
		if get_cell_item(cell.x, cell.y, cell.z) == CROSSING:
			_add_traffic_nodes(cell, crossing_lanes)
			_add_traffic_nodes(cell, offroad_check_crossing)
		# Straight Street and crossroad
		if get_cell_item(cell.x, cell.y, cell.z) == STRAIGHT:
			_add_traffic_nodes(cell, straight_lanes)
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, offroad_check_straight)
		if get_cell_item(cell.x, cell.y, cell.z) == T_CROSSING_STRAIGHT_ONLY:
			_add_traffic_nodes(cell, straight_lanes)
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, offroad_check_tcrossing)
		if get_cell_item(cell.x, cell.y, cell.z) == CROSSROAD:
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, crossing_lanes)
			_add_traffic_nodes(cell, offroad_check_crossing)
		# Curve 
		if get_cell_item(cell.x, cell.y, cell.z) == CURVE:
			_add_traffic_nodes(cell, curve_lanes) 
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, offroad_check_curve)
		# Highway appraoch
		if get_cell_item(cell.x, cell.y, cell.z) == HIGHWAY_APPROACH:
			_add_traffic_nodes(cell, highway_approach_lanes)
			_add_traffic_nodes(cell, offroad_check_road_straight)
		# Highway Straight
		if get_cell_item(cell.x, cell.y, cell.z) == HIGHWAY or get_cell_item(cell.x, cell.y, cell.z) == HIGHWAY_GRASS: 
			_add_traffic_nodes(cell, highway_lanes)
			_add_traffic_nodes(cell, highway_speed_limit)
			_add_traffic_nodes(cell, offroad_check_road_straight)
		# No cars
		if get_cell_item(cell.x, cell.y, cell.z) == NO_CARS_STRAIGHT or get_cell_item(cell.x, cell.y, cell.z) == NO_CARS_CURVE:
			_add_traffic_nodes(cell, ai_cars_forbidden_helper)
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, offroad_check_straight)
		# One closed crossing
		if get_cell_item(cell.x, cell.y, cell.z) == ONE_CLOSED_CROSSING:
			_add_traffic_nodes(cell, tcrossing_lanes)
			_add_traffic_nodes(cell, city_speed_limit)
			_add_traffic_nodes(cell, offroad_check_crossing)
		if get_cell_item(cell.x, cell.y, cell.z) == PLAIN:
			_add_traffic_nodes(cell, city_speed_limit)


func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	#if traffic_node_instance.
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
		traffic_node_instance.rotation_degrees.y += -90
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
		traffic_node_instance.rotation_degrees.y += -180
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
		traffic_node_instance.rotation_degrees.y += -270
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
		traffic_node_instance.rotation_degrees.y += 0
	traffic_node_instance.translation = map_to_world(cell.x, cell.y, cell.z)
	add_child(traffic_node_instance)


func _on_GPS_path_ready(path):
	var max_instances = 0
	for points in navigation_points:
		points.queue_free()
	navigation_points.clear() # Clear the list because else it tries to queue_free empty instances
	for points in path:
		if max_instances > 75: # Limit instances for performance
			return
		else:
			var cell = world_to_map(points)
			var instance = gps_route_indicator.instance()
			instance.translation = map_to_world(cell.x, cell.y, cell.z)
			add_child(instance)
			navigation_points.append(instance)
			max_instances += 1
