extends Node

var traffic_block_collision_bit = 14
# Called when the node enters the scene tree for the first time.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# If the straight path or the crossing one has a car on it, block it
	if get_child(1).get_child_count() > 0 or get_child(3).get_child_count() > 0 or get_child(2).get_child_count() > 0:
		# Have to be set false in editor
		$PriorityRight.set_collision_layer_bit(traffic_block_collision_bit, true)
		#$PriorityRight/MeshInstance.show()
		#$CrossingBlock.set_collision_layer_bit(3, true)
		#$CrossingBlock/MeshInstance.show()
	else:
		$PriorityRight.set_collision_layer_bit(traffic_block_collision_bit, false)
		#$CrossingBlock.set_collision_layer_bit(3, false)
		$CrossingBlock/MeshInstance.hide()
		$PriorityRight/MeshInstance.hide()
