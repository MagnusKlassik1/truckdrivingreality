extends Spatial

export var north_traffic_red = false
export var south_traffic_red = false
export var east_traffic_red = true
export var west_traffic_red = true

onready var area_north = $AreaNorth
onready var area_south = $AreaSouth
onready var area_east = $AreaEast
onready var area_west = $AreaWest

var lights_list_pre = []
var lights_list_post = []
var areas_list = []

export var time_to_switch: int = 3
export var time_being_green: int = 10

var green_light_mat = SpatialMaterial.new()
var yellow_light_mat= SpatialMaterial.new()
var red_light_mat = SpatialMaterial.new()
var no_light_mat = SpatialMaterial.new()

var north_block_wait
var west_block_wait

var traffic_light_collision_layer = 12

func _ready():
	# Set up material
	green_light_mat.albedo_color = Color.chartreuse #Color(112, 242, 114, 78)
	red_light_mat.albedo_color = Color.crimson #Color(112, 196, 242, 78)
	yellow_light_mat.albedo_color = Color.gold
	no_light_mat.albedo_color = Color.dimgray
	# Set up collision layer according to export vars
	area_north.set_collision_layer_bit(traffic_light_collision_layer, north_traffic_red)
	area_south.set_collision_layer_bit(traffic_light_collision_layer, south_traffic_red)
	area_east.set_collision_layer_bit(traffic_light_collision_layer, east_traffic_red)
	area_west.set_collision_layer_bit(traffic_light_collision_layer, west_traffic_red)
	# Create lists
	lights_list_pre = [north_traffic_red, south_traffic_red, east_traffic_red, west_traffic_red]
	lights_list_post = [north_traffic_red, south_traffic_red, east_traffic_red, west_traffic_red]
	areas_list = [area_north, area_south, area_east, area_west]
	# Give color to lights
	var i = 0
	for lights in lights_list_pre:
		if lights == true:
			areas_list[i].red_light.material_override = red_light_mat
			areas_list[i].yellow_light.material_override = no_light_mat
			areas_list[i].green_light.material_override = no_light_mat
		else:
			areas_list[i].red_light.material_override = no_light_mat
			areas_list[i].yellow_light.material_override = no_light_mat
			areas_list[i].green_light.material_override = green_light_mat
		i += 1

func _on_TrafficLightTimer_timeout():
	_switch_traffic_lights()

func _switch_traffic_lights():
	var i = 0
	for lights in lights_list_pre:
		if lights == false:
			areas_list[i].set_collision_layer_bit(traffic_light_collision_layer, true)
			$TrafficYellowTimer.start(2)
#			# Switch traffic light color			
			areas_list[i].red_light.material_override = red_light_mat
			areas_list[i].yellow_light.material_override = no_light_mat
			areas_list[i].green_light.material_override = no_light_mat
			lights_list_post[i] = true
		i += 1
	$TrafficSwitchTimer.start(time_to_switch)

func _on_TrafficSwitchTimer_timeout():
	var i = 0
	for lights in lights_list_pre:
		if lights == true:
			areas_list[i].set_collision_layer_bit(traffic_light_collision_layer, false)
			areas_list[i].red_light.material_override = no_light_mat
			areas_list[i].yellow_light.material_override = no_light_mat
			areas_list[i].green_light.material_override = green_light_mat
			lights_list_post[i] = false
		i += 1
	$TrafficLightTimer.start(time_being_green)
	var j = 0
	# Rewrite traffic light list
	for entries in lights_list_post:
		lights_list_pre[j] = entries
		j += 1
		
func _on_TrafficYellowTimer_timeout():
	var i = 0
	for lights in lights_list_post:
		areas_list[i].yellow_light.material_override = no_light_mat
