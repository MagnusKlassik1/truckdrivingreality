extends Spatial

export var max_speed: int = 50
export var highway_speeding_fine: int = 20

func _ready():
	PlayerManager.highway_speeding_fine = highway_speeding_fine

func _on_Area_body_entered(body):
	# Back on street
	PlayerManager.player_on_street(true)
	# Speeding logic
	PlayerManager.current_allowed_speed = max_speed
	if PlayerManager.current_km_per_hour > max_speed:
		PlayerManager.add_to_highway_speeding_count(1)
	else:
		PlayerManager.add_to_highway_speeding_count(-1)
