extends Spatial

var green_light_mat = SpatialMaterial.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	green_light_mat.albedo_color = Color.gray
	change_color()

func change_color():
	$MeshInstance.material_override = green_light_mat
