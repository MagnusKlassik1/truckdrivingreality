extends Spatial

var car_is_in_area

onready var _description = $CanvasLayer/CenterContainer/VBoxContainer/Description
onready var _buy_sound = $BuyAudio
onready var _error_sound = $ErrorAudio
onready var _popup_sound = $ConfirmAudio

export var food_cost: int = 2
export var drink_cost: int = 1

func _ready():
	set_process(false)
	

func _process(delta):
	if car_is_in_area:
		if PlayerManager.parking_brake_activated:
			_description.text = "Press [E] to buy food. One food costs " + String(food_cost) + "$. \n Press [Q] to buy drinks. One drink costs " + String(drink_cost) + "$."
			_description.show()
			if Input.is_action_just_pressed("player_interact"):
				if PlayerManager.current_money >= food_cost:
					_buy_sound.play()
					PlayerManager.add_food()
					PlayerManager.change_money(-food_cost)
				else:
					$ErrorAudio.play()
					PlayerManager.change_money(0) # To play the money anim
			if Input.is_action_just_pressed("player_secondary_action"):
				if PlayerManager.current_money >= drink_cost:
					_buy_sound.play()
					PlayerManager.add_drink()
					PlayerManager.change_money(-drink_cost)
				else:
					$ErrorAudio.play()
					PlayerManager.change_money(0) # To play the money anim
		else:
			_description.text = "Activate the parking brake to shop."


func _on_Area_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		car_is_in_area = true
		_description.text = "Activate the parking brake to shop."
		_description.show()
		_popup_sound.play()


func _on_Area_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		car_is_in_area = false
		_description.hide()
