extends Spatial

var car_is_in_area: bool = false
var repair_points: int = 0

export var _cost_per_point: float = 1
onready var _repair_settings = $CanvasLayer/CenterContainer/VBoxContainer/RepairSettings
onready var _description = $CanvasLayer/CenterContainer/VBoxContainer/Description
onready var _reapir_sound = $RepairAudio
onready var _animation_player = $AnimationPlayer

func _ready():
	set_process(false)
	


func _process(delta):
	if car_is_in_area:
		if PlayerManager.parking_brake_activated:
			_repair_settings.show()
			#_description.show()
			if PlayerManager.damage_report == 100:
				_description.text = "Your truck is in top condition and needs no repair."
				car_is_in_area = false
				_repair_settings.hide()
			else:
				_description.text = "Your car is " + String(PlayerManager.damage_report) + "% intact. Every percent will cost " + String(_cost_per_point) + "$"
			if Input.is_action_just_pressed("ui_left"):
				if repair_points > 0:
					#_select_sound.play()
					repair_points -= 1
			if Input.is_action_just_pressed("ui_right"):
				#_select_sound.play()
				repair_points += 1
			if repair_points == 0:
				_repair_settings.text = String(repair_points) + " >"
			else: 
				_repair_settings.text = "< " + String(repair_points) + " >"
			if Input.is_action_just_pressed("player_interact"):
				if PlayerManager.current_money >= repair_points*_cost_per_point:
					car_is_in_area = false # Do this to hide text during animation
					_animation_player.play("RepairAnim")
					#_confirm_sound.play()
					_reapir_sound.play()
					PlayerManager.repair(repair_points)
					PlayerManager.change_money(-repair_points*_cost_per_point)
					repair_points = 0
				else:
					return
		else:
			_repair_settings.hide()
			_description.text = "Activate the parking brake to repair your car."
			

func _on_Area_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		car_is_in_area = true
		_description.text = "Activate the parking brake to repair your car."
		_description.show()
		#_popup_sound.play()


func _on_Area_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		car_is_in_area = false
		_description.hide()





func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "RepairAnim":
		car_is_in_area = true
