extends Spatial



var car_is_in_area
var screen_is_active = false

export var costs_per_use: float = 0

onready var _description = $CanvasLayer/CenterContainer/Description
onready var _flush_sound = $Flush
onready var _popup_sound = $PopupSound
onready var _animation_player = $AnimationPlayer
onready var _error_sound = $Error
# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if car_is_in_area:
		if PlayerManager.parking_brake_activated:
			#_popup_sound.play()
			_description.text = "Using the bathroom will cost " + String(costs_per_use) + "$ and take 5 minutes. \n Press [E] to use the bathroom."
			_description.show()
			if Input.is_action_just_pressed("player_interact"):
				if PlayerManager.current_money >= costs_per_use:
					PlayerManager.change_money(-costs_per_use)
					var value = PlayerManager.max_needs_value - PlayerManager.bladder
					PlayerManager.add_bladder(value)				
					WorldManager._add_time_to_world(0,5)
					_flush_sound.play()
					_animation_player.play("ToiletAnim")
				else:
					_error_sound.play()
					#return
		else:
			_description.text = "Activate the parking brake to use the rest room."
			

func _on_Area_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		car_is_in_area = true
		_description.text = "Activate the parking brake to use the rest room."
		_description.show()
		_popup_sound.play()


func _on_Area_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		car_is_in_area = false
		_description.hide()
