extends Spatial

var car_is_in_area = false
var screen_is_active = false
var amount_of_gas = 0


onready var info_box = $CanvasLayer/CenterContainer/VBoxContainer
onready var gas_amount_text = $CanvasLayer/CenterContainer/VBoxContainer/AmountOfGas
onready var _description = $CanvasLayer/CenterContainer/VBoxContainer/Description
onready var _company_paid = $CanvasLayer/CenterContainer/VBoxContainer/CompanyPaid
onready var _popup_sound = $SelectAudio
onready var _refuel_sound = $RefuelAudio
onready var _animation_player = $AnimationPlayer


export var gas_price: float = 0

func _ready():
	set_process(false)

func _process(delta):
	#dollar.rotate_y(delta)
	if car_is_in_area:
		if PlayerManager.parking_brake_activated:
			gas_amount_text.show()
			_description.text = "Set the amount of gas to buy or press [secondary action] to fill up completely."
			_company_paid.show()
			if Input.is_action_just_pressed("ui_left"):
				if amount_of_gas > 0:
					amount_of_gas -= 1
			if Input.is_action_just_pressed("ui_right"):
				amount_of_gas += 1
			if amount_of_gas == 0:
				gas_amount_text.text = String(amount_of_gas) + " >"
			else: 
				gas_amount_text.text = "< " + String(amount_of_gas) + " >"
			if Input.is_action_just_pressed("player_secondary_action"):
				amount_of_gas = PlayerManager.max_needs_value - PlayerManager.gas
			if Input.is_action_just_pressed("player_interact"):
				WorldManager._add_time_to_world(0,5)
				PlayerManager.add_gas(amount_of_gas)
				PlayerManager.change_money(-amount_of_gas*gas_price)
				amount_of_gas = 0
				_refuel_sound.play()
				_animation_player.play("Refueling")
				car_is_in_area = false
		else:
			gas_amount_text.hide()
			_description.text = "Activate the parking brake to refuel."
			_company_paid.hide()


func _on_Area_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		car_is_in_area = true
		_description.text = "Activate the parking brake to refuel."
		_description.show()
		_popup_sound.play()


func _on_Area_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		car_is_in_area = false
		amount_of_gas = 0
		_description.hide()


func _on_AnimationPlayer_animation_finished(anim_name):
	car_is_in_area = true
