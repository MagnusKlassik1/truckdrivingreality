extends Spatial


var car_is_in_area = false
var screen_is_active = false
var amount_of_sleep = 0

export var costs_per_hour: float = 0

onready var sleep_hours_text = $CanvasLayer/CenterContainer/VBoxContainer/Hours
onready var _description = $CanvasLayer/CenterContainer/VBoxContainer/Description
onready var info_box = $CanvasLayer/CenterContainer/VBoxContainer
onready var _animation_player = $AnimationPlayer
onready var _sleeping_sound = $Sleeping
onready var _select_sound = $Select
onready var _popup_sound = $Popup
onready var _confirm_sound = $Confirm
onready var _error_sound = $Error


func _ready():
	set_process(false)

func _process(delta):
	if car_is_in_area:
		if PlayerManager.parking_brake_activated:
			sleep_hours_text.show()
			#_description.show()
			_description.text = "Set the hours to sleep. Every hour will cost " + String(costs_per_hour) + "$. \n Press [E] to sleep."
			if Input.is_action_just_pressed("ui_left"):
				if amount_of_sleep > 0:
					_select_sound.play()
					amount_of_sleep -= 1
			if Input.is_action_just_pressed("ui_right"):
				_select_sound.play()
				amount_of_sleep += 1
			if amount_of_sleep == 0:
				sleep_hours_text.text = String(amount_of_sleep) + " >"
			else: 
				sleep_hours_text.text = "< " + String(amount_of_sleep) + " >"
			if Input.is_action_just_pressed("player_interact"):
				if PlayerManager.current_money >= amount_of_sleep*costs_per_hour:
					car_is_in_area = false # Do this to hide text during animation
					_animation_player.play("SleepAnim")
					_confirm_sound.play()
					_sleeping_sound.play()
					PlayerManager.sleep(amount_of_sleep)
					PlayerManager.change_money(-amount_of_sleep*costs_per_hour)
					WorldManager._add_time_to_world(amount_of_sleep,0)
					MissionManager.mission_clock._set_time(MissionManager.mission_clock.add_time(0,amount_of_sleep,0))
					amount_of_sleep = 0
				else:
					_error_sound.play()
		else:
			sleep_hours_text.hide()
			_description.text = "Activate the parking brake to rest."
			
		
		
func _on_Area_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		car_is_in_area = true
		_description.text = "Activate the parking brake to rest."
		_description.show()
		_popup_sound.play()


func _on_Area_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		car_is_in_area = false
		_description.hide()
		amount_of_sleep = 0


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "SleepAnim":
		car_is_in_area = true
