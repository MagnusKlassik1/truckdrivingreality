extends Spatial

var default_color = Color(0, 0.51, 1)
var in_place_mat = SpatialMaterial.new()
var out_place_mat = SpatialMaterial.new()



onready var _description = $CanvasLayer/CenterContainer/VBoxContainer/Description
onready var _hud = $CanvasLayer/CenterContainer/VBoxContainer
onready var _animation_player = $AnimationPlayer
onready var _garage_sound = $GarageDoor
onready var _select_audio = $SelectAudio
onready var _validate_audio = $ValidateAudio
onready var _beginning_music = $BeginningMusic

var _car_is_in_company = false
var _delivery_unloaded = false
var _delivery_point_reached = false

# This will be set in the script where the company instances will be 
# instanced!!
var _new_game = false 

func _ready():	
	# Don't ask me why I have to hide the ColorRect for the opening sequence
	# to be played correctly.
	$CanvasLayer/ColorRect.hide()
	MissionManager.connect("mission_succeed", self, "_mission_success")
	MissionManager.connect("mission_failure", self, "_mission_failure")
	in_place_mat.albedo_color = Color.green #Color(112, 242, 114, 78)
	out_place_mat.albedo_color = Color.floralwhite #Color(112, 196, 242, 78)
	set_process(false)

func _process(delta):
	if Input.is_action_just_pressed("player_interact"):
		if !_new_game:
			if _car_is_in_company:
				if _description.visible:
					_validate_audio.play()
					_description.hide()
					_animation_player.play("DoorOpen")
			if _delivery_point_reached:
				MissionManager.interact()
				_validate_audio.play()
				_description.hide()
				# Either way the mission is accepted, thus the car
				# will not remain in the 'right' company 
				_delivery_point_reached = false
		else:
			if _description.visible:
				MissionManager.start_first_mission()
				#_validate_audio.play()
				_animation_player.play("DoorOpen")
				_beginning_music.play()
				_description.hide()
				_new_game = false


func _on_AnimationPlayer_animation_started(anim_name):
	if anim_name == "DoorOpen":
		_garage_sound.play()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "OpeningSequence":
		_description.text = "Hello, new Truck Driver. You may now start your first delivery! \n \n However, it is important that you deliver your shipments on time. \n In the upper left corner you see the current time (CT) and estimated time of arrival (ETA) after leaving this company. \n \n Press [E] to continue. \n \n "
		_description.show()
		_select_audio.play()


func _on_CompanyArea_body_entered(body):
	if body.name == "Cab":
		set_process(true)
		if !_new_game:
			# Check if this is the right company by comparing its position to current
			# destination
			if translation == MissionManager.current_destination:
				_car_is_in_company = true
				_description.text = "Drive through door nr. 1 and unload your delivery. \n Press [E] to continue."
				_description.show()
				_select_audio.play()
		if _new_game:
			#if _car_is_in_company:
				#if !_animation_player.is_playing():
			_start_opening_sequence()
			# Do this because else the money label will be red
			# which seems to be a bug of the animation_player
			PlayerManager.change_money(0)


func _on_CompanyArea_body_exited(body):
	if body.name == "Cab":
		set_process(false)
		_car_is_in_company = false
		_animation_player.play("DoorsClose")


func _on_Door1Area_body_entered(body):
	if body.name == "Cab":
		if !_new_game:
			_description.text = "Press [E] to unload your truck. A new delivery will start immediately afterswards."
			_description.show()
			_delivery_point_reached = true
			_car_is_in_company = false


func _start_opening_sequence():
	_animation_player.play("OpeningSequence")


func _mission_success():
	$CashAudio.play()


func _mission_failure():
	PlayerManager.add_failed_mission()
	$FailAudio.play()
