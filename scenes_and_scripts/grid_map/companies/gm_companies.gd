extends GridMap

onready var company_logic = preload("res://scenes_and_scripts/grid_map/companies/company.tscn")
var company_list = []

const NORTH = 10
const WEST = 22
const EAST = 16
const SOUTH = 0

func _ready():
	for cell in get_used_cells():
		# Instance logic
		var company_logic_instance = company_logic.instance()
		var trans = map_to_world(cell.x, cell.y, cell.z)
		company_logic_instance.translation = trans
		# Orientation
		if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
			company_logic_instance.rotation_degrees.y += -90
		if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
			company_logic_instance.rotation_degrees.y += -180
		if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
			company_logic_instance.rotation_degrees.y += -270
		if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
			company_logic_instance.rotation_degrees.y += 0
		add_child(company_logic_instance)
		# Add adress here because we need position in world
		# In the scene itself we would get the local position 0
		MissionManager.adress_list.append(trans)
		company_list.append(company_logic_instance)
	# Company #6 is where the game starts and where this variable has to
	# be set true
	company_list[5]._new_game = true




func get_player_on_company_gridmap():
	PlayerManager.current_company_tile = world_to_map(PlayerManager.player_position)
