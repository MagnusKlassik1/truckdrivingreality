extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var main_menu = preload("res://scenes_and_scripts/level/main_menu.tscn")
onready var _introduction_color_rect = $VBoxContainer/Introduction/IntroColorRect
onready var _introduction_text = $VBoxContainer/Introduction/Intro

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("player_close"):
		if !visible:
			get_tree().paused = true
			Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
			show()
		else:
			get_tree().paused = false
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			hide()


func _on_TowingService_pressed():
	pass


func _on_Continue_pressed():
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hide()


func _on_Main_Menu_button_down():
	get_tree().paused = false
	get_tree().change_scene("res://scenes_and_scripts/level/main_menu.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_Introduction_pressed():
	if _introduction_color_rect.visible:
		_introduction_color_rect.hide()
	else:
		_introduction_color_rect.show()
	if _introduction_text.visible:
		_introduction_text.hide()
	else:
		_introduction_text.show()


func _on_OptionsButton_pressed():
	pass # Replace with function body.
