extends Spatial

onready var red_light = $Pillar/Fixture/RedLight
onready var yellow_light = $Pillar/Fixture/RedLight/YellowLight
onready var green_light = $Pillar/Fixture/RedLight/GreenLight

onready var green_light_timer = $GreenLightTimer
onready var yellow_light_timer = $YellowLightTimer
onready var red_light_timer = $RedLightTimer
onready var wait_timer = $WaitTimer

onready var stop_area = $StopArea
onready var monitoring_timer = $MonitoringTimer
onready var popup = $CanvasLayer/CenterContainer/Popup
onready var _punishment_sound = $PunishmentAudio

var green_light_mat = SpatialMaterial.new()
var yellow_light_mat= SpatialMaterial.new()
var red_light_mat = SpatialMaterial.new()
var no_light_mat = SpatialMaterial.new()

export var red: bool = false

export var red_time: int = 10
export var yellow_time: int = 2
export var green_time: int = 10
export var switch_time: int = 5
export var driving_through_red_light_fine: float = 10

var traffic_block_collision_bit = 14

var position 
const FRONT = 1
const BACK = 2

func _ready():
	# Colors
	green_light_mat.albedo_color = Color.chartreuse
	red_light_mat.albedo_color = Color.crimson 
	yellow_light_mat.albedo_color = Color.gold
	no_light_mat.albedo_color = Color.dimgray
	# Give same colors
	red_light.material_override = no_light_mat
	yellow_light.material_override = no_light_mat
	green_light.material_override = no_light_mat
	# setup
	if red:
		red_light_timer.start(red_time)
		red_light.material_override = red_light_mat
		# Collision
		stop_area.set_collision_layer_bit(traffic_block_collision_bit, true)
	else:
		green_light_timer.start(green_time)
		green_light.material_override = green_light_mat


func _on_GreenLightTimer_timeout():
	yellow_light_timer.start(yellow_time)
	yellow_light.material_override = yellow_light_mat
	green_light.material_override = no_light_mat


func _on_YellowLightTimer_timeout():
	# Collision
	stop_area.set_collision_layer_bit(traffic_block_collision_bit, true)
	# Color
	red_light_timer.start(red_time)
	red_light.material_override = red_light_mat
	yellow_light.material_override = no_light_mat
	red = true

func _on_RedLightTimer_timeout():
	wait_timer.start(switch_time)

func _on_WaitTimer_timeout():
	# Collision
	stop_area.set_collision_layer_bit(traffic_block_collision_bit, false)
	# Color
	green_light_timer.start(green_time)
	green_light.material_override = green_light_mat
	red_light.material_override = no_light_mat
	red = false

func _on_StopArea_body_entered(body):
	if body.name == "Cab":
		# Disable monitoring to prevent red traffic lights which the player
		# can legally pass from sanctioning the player
		if stop_area.get_collision_layer_bit(traffic_block_collision_bit):
			if position == FRONT:
				PlayerManager.change_money(-driving_through_red_light_fine)
				_punishment_sound.play()
				$CanvasLayer/CenterContainer/Popup/Label.text = "Red light violiation! Fine: " + String(driving_through_red_light_fine) + "$"
				popup.show()
				position = BACK # else you can drive through red 
		monitoring_timer.start(3)

func _on_MonitoringTimer_timeout():
	popup.hide()
	#stop_area.set_deferred("monitoring", false)


func _on_FrontStopArea_body_entered(body):
	if body.name == "Cab":
		position = FRONT


func _on_BackStopArea_body_entered(body):
	if body.name == "Cab":
		position = BACK
