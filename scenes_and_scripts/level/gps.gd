extends Navigation

signal path_ready(path)

onready var path
onready var nav_update_timer = $NavUpdateTimer
onready var streets_gm = $NavigationMeshInstance/Streets
var distance_in_meters = 0
var distance_in_kilometers = 0
var tile_size: int = 10
var meter_in_kilometer_factor: float = 0.001
var distance_calculated: bool = false


# Called when the node enters the scene tree for the first time.
func _ready():
	MissionManager.connect("update_mission", self, "create_path")
	MissionManager.connect("calculate_ETA", self, "calculate_ETA")

func create_path(current, start, dest, dist):
	path = get_simple_path(PlayerManager.global_player_position, MissionManager.current_destination)
	if path.size() < 1:
		if PlayerManager.is_on_street:
			print("Path was < 1")
			print("Path was meant to: " + String(MissionManager.current_destination))
			MissionManager.start_new_mission()
	else:
		emit_signal("path_ready", path)
		#print(path)
		# Somehow it is much more precise when using half the cell size for distance
		distance_in_meters = path.size()*(streets_gm.cell_size.x/2)
		distance_in_kilometers = distance_in_meters*meter_in_kilometer_factor
		if distance_calculated == false:
			MissionManager.distance = distance_in_kilometers
			distance_calculated = true
		MissionManager.distance_left = distance_in_meters*meter_in_kilometer_factor
		# Start timer here since before there is no distance
		nav_update_timer.start()


func _on_Timer_timeout():
	MissionManager.update_mission_navigation()
	

func calculate_ETA():
	# Also the distance has to be newly calculated
	# Todo: make it in an extra function
	distance_calculated = false
	var time_left_in_seconds: float = distance_in_meters / MissionManager.expected_meters_per_second
	var time_left_in_minutes = time_left_in_seconds / 60
	# Get current time first and add minutes to goal. Adding hours and days
	# will be done by the logic within the clock.
	MissionManager.mission_clock._set_time(WorldManager.clock.get_current_time())
	MissionManager.mission_clock._set_time(MissionManager.mission_clock.add_time(0,0, time_left_in_minutes))
