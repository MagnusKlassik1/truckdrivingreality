extends GridMap

onready var rest_area = preload("res://scenes_and_scripts/grid_map/needs/rest_area.tscn")
onready var rest_room = preload("res://scenes_and_scripts/grid_map/needs/rest_room.tscn")
onready var gas_station = preload("res://scenes_and_scripts/grid_map/needs/gas_station.tscn")
onready var grocery_shop = preload("res://scenes_and_scripts/grid_map/needs/grocery_shop.tscn")
onready var car_repair_shop = preload("res://scenes_and_scripts/grid_map/needs/car_repair_shop.tscn")

# orientation 
const NORTH = 10
const WEST = 22
const EAST = 16
const SOUTH = 0

func _ready():
	for cell in get_used_cells():
		if get_cell_item(cell.x, cell.y, cell.z) == 0:
			_instance_tile(grocery_shop, cell)
		if get_cell_item(cell.x, cell.y, cell.z) == 1:
			_instance_tile(rest_room, cell)
		if get_cell_item(cell.x, cell.y, cell.z) == 2:
			_instance_tile(gas_station, cell)
		if get_cell_item(cell.x, cell.y, cell.z) == 3:
			_instance_tile(rest_area, cell)
		if get_cell_item(cell.x, cell.y, cell.z) == 4:
			_instance_tile(car_repair_shop, cell)

func _instance_tile(tile, cell):
	var tile_instance = tile.instance()
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
		tile_instance.rotation_degrees.y += -90
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
		tile_instance.rotation_degrees.y += -180
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
		tile_instance.rotation_degrees.y += -270
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
		tile_instance.rotation_degrees.y += 0
	var trans = map_to_world(cell.x, cell.y, cell.z)
	tile_instance.translation = trans
	add_child(tile_instance)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
