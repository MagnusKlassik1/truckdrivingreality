extends Spatial

onready var _introduction_color_rect = $Control/CanvasLayer/CenterContainer/VBoxContainer/Introduction/IntroColorRect
onready var _introduction_text = $Control/CanvasLayer/CenterContainer/VBoxContainer/Introduction/Intro
onready var _music = $Music
onready var credits_label = $Control/CanvasLayer/CenterContainer/VBoxContainer/CreditsButton/Credits
onready var _options = $Control/CanvasLayer/CenterContainer/VBoxContainer/OptionsButton/Options
onready var _resolution_options = $Control/CanvasLayer/CenterContainer/VBoxContainer/OptionsButton/Options/Resolution/ResolutionOptions
onready var _environment = $WorldEnvironment
onready var _light = $WorldEnvironment/DirectionalLight
onready var _options_color_rect = $Control/CanvasLayer/CenterContainer/VBoxContainer/OptionsButton/OptionsColorRect
onready var _msaa_options = $Control/CanvasLayer/CenterContainer/VBoxContainer/OptionsButton/Options/Resolution/MSAACheckBox


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	# Res
	_resolution_options.add_item("1280 x 1024")
	_resolution_options.add_item("1366 x 768")
	_resolution_options.add_item("1600 x 900")
	_resolution_options.add_item("1920 x 1080")
	_resolution_options.add_item("1920 x 1200")
	_resolution_options.add_item("2560 x 1440")
	_resolution_options.selected = 3
	# MSAA
	_msaa_options.add_item("MSAA: disabled")
	_msaa_options.add_item("MSAA: 2x")
	_msaa_options.add_item("MSAA: 4x")
	_msaa_options.add_item("MSAA: 8x")
	_msaa_options.add_item("MSAA: 16x")
	_msaa_options.selected = 2
	# Music
	_music.play()


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_StartButton_pressed():
	get_tree().change_scene("res://scenes_and_scripts/level/main.tscn")


func _on_CreditsButton_pressed():
	get_tree().change_scene("res://scenes_and_scripts/hud/Credits.tscn")


func _on_Area_body_entered(body):
	body.get_parent().set_physics_process(true)


func _on_MouseSlider_value_changed(value):
	PlayerManager.mouse_sensitivity = value


func _on_OptionsButton_pressed():
	if _options.visible:
		_options.hide()
	else:
		_options.show()
	if _options_color_rect.visible:
		_options_color_rect.hide()
	else:
		_options_color_rect.show()

func _on_ResolutionOptions_item_selected(index):
	if index == 0:
		OS.window_size = Vector2(1280, 1024)
	if index == 1:
		OS.window_size = Vector2(1366, 768)
	if index == 2:
		OS.window_size = Vector2(1600, 900)
	if index == 3:
		OS.window_size = Vector2(1920, 1080)
	if index == 4:
		OS.window_size = Vector2(1920, 1200)
	if index == 5:
		OS.window_size = Vector2(2560, 1440)


func _on_CheckBox_toggled(button_pressed):
	if button_pressed:
		OS.window_fullscreen = true
	else:
		OS.window_fullscreen = false


func _on_ShadowsCheckBox_toggled(button_pressed):
	if button_pressed:
		_light.shadow_enabled = true
	else:
		_light.shadow_enabled = false


func _on_SSAO_toggled(button_pressed):
	if button_pressed:
		_environment.environment.ssao_enabled = true
	else:
		_environment.environment.ssao_enabled = false


func _on_MSAACheckBox_item_selected(index):
	if index == 0:
		get_viewport().msaa = Viewport.MSAA_DISABLED
	if index == 1:
		get_viewport().msaa = Viewport.MSAA_2X
	if index == 2:
		get_viewport().msaa = Viewport.MSAA_4X
	if index == 3:
		get_viewport().msaa = Viewport.MSAA_8X
	if index == 4:
		get_viewport().msaa = Viewport.MSAA_16X


func _on_Introduction_pressed():
	if _introduction_color_rect.visible:
		_introduction_color_rect.hide()
	else:
		_introduction_color_rect.show()
	if _introduction_text.visible:
		_introduction_text.hide()
	else:
		_introduction_text.show()
