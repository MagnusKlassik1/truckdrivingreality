extends Spatial

export var starting_hour: int = 0
export var starting_minute: int = 0


var game_menu = preload("res://scenes_and_scripts/level/game_menu.tscn")
# Begin the day at a 8 o clock
func _ready():
	WorldManager._add_time_to_world(starting_hour,starting_minute)

func _process(delta):
	# This is only for debug-reasons
	if Input.is_action_just_pressed("ui_page_up"):
		print("new mission")
		MissionManager.start_new_mission()
