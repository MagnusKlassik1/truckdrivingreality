extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var _lost_audio = $LostAudio
onready var _distance_text = $CenterContainer/VBoxContainer/DistanceTraveled
# Called when the node enters the scene tree for the first time.
func _ready():
	_lost_audio.play()
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	_distance_text.text = "Distance driven: " + String(stepify(PlayerManager.distance_driven, 0.1)) + " m. \n"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("player_close"):
		get_tree().change_scene("res://scenes_and_scripts/level/main_menu.tscn")


func _on_LostAudio_finished():
	_lost_audio.stop()


func _on_Button_pressed():
	get_tree().change_scene("res://scenes_and_scripts/level/main_menu.tscn")
