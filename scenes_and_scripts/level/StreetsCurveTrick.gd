extends GridMap

var gps_big_curve_component = preload("res://scenes_and_scripts/grid_map/gps/gps_big_curve_component.tscn")
onready var highway_big_curve_lanes = preload("res://scenes_and_scripts/grid_map/streets/countryroad_big_curve_lanes.tscn")
onready var offroad_check_road_curve = preload("res://scenes_and_scripts/grid_map/streets/offroad_check_countryroad_curve.tscn")
onready var speed_limit = preload("res://scenes_and_scripts/grid_map/traffic/highway_speed_limit_curve.tscn")

const COUNTRYROAD_BIG_CURVE = 20

# orientation 
const NORTH = 10
const WEST = 22
const EAST = 16
const SOUTH = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	for cell in get_used_cells():
		# Navigation System
		if get_cell_item(cell.x, cell.y, cell.z) == COUNTRYROAD_BIG_CURVE:
			_add_traffic_nodes(cell, gps_big_curve_component)
			_add_traffic_nodes(cell, highway_big_curve_lanes)
			_add_traffic_nodes(cell, offroad_check_road_curve)
			_add_traffic_nodes(cell, speed_limit)

func _add_traffic_nodes(cell, traffic_node):
	var traffic_node_instance = traffic_node.instance()
	#if traffic_node_instance.
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == WEST:
		traffic_node_instance.rotation_degrees.y += -90
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == NORTH:
		traffic_node_instance.rotation_degrees.y += -180
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == EAST:
		traffic_node_instance.rotation_degrees.y += -270
	if get_cell_item_orientation(cell.x, cell.y, cell.z) == SOUTH:
		traffic_node_instance.rotation_degrees.y += 0
	traffic_node_instance.translation = map_to_world(cell.x, cell.y, cell.z)
	add_child(traffic_node_instance)
